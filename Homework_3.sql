-- 1. Всіх діючих співробітників розбийте на сегменти залежно від віку в момент прийому на роботу:
-- до 25, 25-44, 45-54, 55 і старше, для кожного сегменту виведіть максимальну зарплату. В результаті
-- потрібно отримати два поля сегмент, максимальну зарплату в сегменті.

SELECT CASE WHEN TIMESTAMPDIFF(YEAR, ee.birth_date, ee.hire_date) < 25 THEN '< 25' 
			WHEN TIMESTAMPDIFF(YEAR, ee.birth_date, ee.hire_date) BETWEEN 25 AND 44 THEN '25-44' 
			WHEN TIMESTAMPDIFF(YEAR, ee.birth_date, ee.hire_date) BETWEEN 45 AND 54 THEN '45-54'
			ELSE '>= 55' 
		END AS hiring_age,
		MAX(es.salary) AS max_salary
FROM employees.employees AS ee
JOIN employees.salaries AS es ON ee.emp_no = es.emp_no
WHERE es.to_date > CURDATE()
GROUP BY hiring_age
ORDER BY hiring_age;

-- 2. Покажіть посаду та зарплату працівника з найвищою зарплатою більше не працюючого в компанії.
SELECT et.title, es.salary
FROM employees.salaries AS es
JOIN employees.titles AS et ON es.emp_no = et.emp_no AND et.to_date < CURDATE()
WHERE es.salary = (SELECT MAX(salary)
					FROM employees.salaries
                    WHERE to_date < CURDATE()) AND es.to_date < CURDATE()
LIMIT 1;
-- Не розумію чому сума інша, ніж у відповідях, якщо перевірити підзапит, то максимальна зп
-- серед непрацюючих у компанії працівників, виходить '157821'
-- Підзапит
SELECT MAX(salary)
FROM employees.salaries
WHERE to_date < CURDATE();

-- 3. Покажіть ТОР-10 діючих співробітників з найбільшою зарплатою
SELECT es.emp_no, CONCAT(ee.first_name, ' ', ee.last_name) AS full_name, MAX(salary) AS salary 
FROM employees.salaries AS es
JOIN employees.employees AS ee ON es.emp_no = ee.emp_no
WHERE  es.to_date > CURDATE()
GROUP BY es.emp_no
ORDER BY MAX(salary) DESC
LIMIT 10;

-- 4. Покажіть діючих співробітників зарплата яких вища ніж середня зарплата по діючим співробітникам
SELECT ee.emp_no, CONCAT(ee.first_name, ' ', ee.last_name) AS full_name, es.salary
FROM employees.salaries AS es
JOIN employees.employees AS ee ON es.emp_no = ee.emp_no
WHERE es.salary > (SELECT ROUND(AVG(es.salary),2) 
					FROM employees.salaries AS es 
                    WHERE es.to_date > CURDATE()) AND es.to_date > CURDATE();
                    
-- 5. Покажіть співробітників, які працюють у відділах де працює більш ніж 20000 співробітників
SELECT ee.emp_no, CONCAT(ee.first_name, ' ', ee.last_name) AS full_name, dp.dept_name
FROM employees.employees AS ee
JOIN employees.dept_emp AS de ON ee.emp_no = de.emp_no
JOIN employees.departments AS dp ON de.dept_no = dp.dept_no
WHERE de.dept_no IN (SELECT dept_no
					FROM employees.dept_emp
                    WHERE to_date > CURDATE()
					GROUP BY dept_no
					HAVING COUNT(emp_no) > 20000) AND de.to_date > CURDATE()
ORDER BY ee.emp_no;

-- 6. Покажіть співробітників, які заробляють більше, ніж будь-який інший працівник відділу Finance
SELECT es.emp_no, es.salary
FROM employees.salaries AS es
WHERE es.salary > (SELECT MAX(es1.salary) AS max_fin_dep
					FROM employees.dept_emp AS de1
					JOIN employees.salaries AS es1 ON de1.emp_no = es1.emp_no AND es1.to_date > CURDATE()
					JOIN employees.departments AS dp1 ON de1.dept_no = dp1.dept_no
					WHERE dp1.dept_name = 'Finance' AND de1.to_date > CURDATE()
                    GROUP BY dp1.dept_name) AND es.to_date > CURDATE();

-- 7. Покажіть назви відділів, де колись працював хоча б один співробітник з зарплатою більше $150K
SELECT DISTINCT dp.dept_name
FROM employees.departments AS dp
JOIN employees.dept_emp AS de ON dp.dept_no = de.dept_no
JOIN employees.salaries AS es ON de.emp_no = es.emp_no 
WHERE es.salary > 150000;

